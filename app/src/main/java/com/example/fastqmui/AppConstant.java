package com.example.fastqmui;

public class AppConstant {
    public static final String DATE_FORMAT_STYLE = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT_STYLE = "yyyy-MM-dd HH:mm";
    public static final String BASIC_URL = "http://192.168.0.186:8080/project/";

}