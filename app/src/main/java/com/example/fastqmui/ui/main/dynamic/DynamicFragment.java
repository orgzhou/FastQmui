package com.example.fastqmui.ui.main.dynamic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.example.fastqmui.R;
import com.example.fastqmui.base.BaseFragment;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import dagger.hilt.android.AndroidEntryPoint;

/**
 * 动态
 */
@AndroidEntryPoint
public class DynamicFragment extends BaseFragment {
    @BindView(R.id.topbar)
    QMUITopBarLayout topbar;

    private DynamicViewModel dynamicViewModel;

    @Override
    public void execute() {
        dynamicViewModel = new ViewModelProvider(this).get(DynamicViewModel.class);

        initTopbar();
        initLiveData();
    }

    @Override
    public int initLayout() {
        return R.layout.fragment_dynamic;
    }

    @Override
    public void initLiveData() {
        dynamicViewModel.getListDynamicMutableLiveData().observe(getViewLifecycleOwner(), dynamics -> {
            int i = 1;
        });
        dynamicViewModel.list("广东", 1, 10);
    }

    @Override
    public void initTopbar() {
        topbar.setTitle("动态");
    }
}
