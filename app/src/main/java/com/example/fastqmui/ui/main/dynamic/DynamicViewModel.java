package com.example.fastqmui.ui.main.dynamic;

import android.util.Log;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.fastqmui.data.dynamic.DynamicRepository;
import com.example.fastqmui.data.dynamic.domain.Dynamic;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.Data;
import lombok.EqualsAndHashCode;

@SuppressWarnings("ALL")
public class DynamicViewModel extends ViewModel {
    private static final String TAG = "DynamicViewModel";
    private MutableLiveData<List<Dynamic>> listDynamicMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<List<Dynamic>> getListDynamicMutableLiveData() {
        return listDynamicMutableLiveData;
    }

    private DynamicRepository dynamicRepository;

    @ViewModelInject
    public DynamicViewModel(DynamicRepository dynamicRepository) {
        this.dynamicRepository = dynamicRepository;
    }

    public void list(String keyword, int page, int size) {
        dynamicRepository.list(keyword, page, size)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(dynamics -> {
                listDynamicMutableLiveData.postValue(dynamics);
            }, throwable -> {
                Log.d(TAG, "list: ", throwable);
            });
    }
}
