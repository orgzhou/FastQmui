package com.example.fastqmui.ui.main.home;

import androidx.drawerlayout.widget.DrawerLayout;

import com.example.fastqmui.R;
import com.example.fastqmui.base.BaseFragment;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;
import com.qmuiteam.qmui.widget.QMUITopBar;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;

import butterknife.BindView;

/**
 * 主页
 */
public class HomeFragment extends BaseFragment {
    @BindView(R.id.topbar)
    QMUITopBarLayout topbar;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigationView)
    NavigationView navigationView;

    @Override
    public void initViews() {

    }

    /**
     * 去掉滚动条
     *
     * @param navigationView navigationView
     */
    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    @Override
    public void execute() {
        disableNavigationViewScrollbars(navigationView);
    }

    @Override
    public int initLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void initTopbar() {
        topbar.setTitle("主页");
        topbar.addLeftImageButton(R.mipmap.icon_cbl, R.id.fastqmui).setOnClickListener(v -> {
            drawerLayout.openDrawer(navigationView);
        });

    }
}
