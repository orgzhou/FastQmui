package com.example.fastqmui.ui.main.message;

import android.view.LayoutInflater;
import android.view.View;

import com.example.fastqmui.R;
import com.example.fastqmui.base.BaseFragment;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageFragment extends BaseFragment {
    @BindView(R.id.topbar)
    QMUITopBarLayout topbar;

    @Override
    public void execute() {

    }

    @Override
    public int initLayout() {
        return R.layout.fragment_message;
    }

    @Override
    public void initTopbar() {
        topbar.setTitle("消息");
    }
}
