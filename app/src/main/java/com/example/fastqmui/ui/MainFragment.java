package com.example.fastqmui.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.core.content.ContextCompat;

import com.example.fastqmui.R;
import com.example.fastqmui.base.BaseFragment;
import com.example.fastqmui.ui.main.channel.ChannelFragment;
import com.example.fastqmui.ui.main.dynamic.DynamicFragment;
import com.example.fastqmui.ui.main.home.HomeFragment;
import com.example.fastqmui.ui.main.message.MessageFragment;
import com.example.fastqmui.ui.main.mine.MineFragment;
import com.qmuiteam.qmui.arch.QMUIFragment;
import com.qmuiteam.qmui.arch.QMUIFragmentPagerAdapter;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.QMUIViewPager;
import com.qmuiteam.qmui.widget.tab.QMUITab;
import com.qmuiteam.qmui.widget.tab.QMUITabBuilder;
import com.qmuiteam.qmui.widget.tab.QMUITabSegment;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends BaseFragment {

    @BindView(R.id.pager)
    QMUIViewPager viewPager;

    @BindView(R.id.tabs)
    QMUITabSegment tabSegment;

    private HashMap<Pager, QMUIFragment> pagers;

    @Override
    public void execute() {
        initTabs();
        initPagers();
    }

    @Override
    public int initLayout() {
        return R.layout.fragment_main;
    }

    private void initTabs() {
        QMUITabBuilder builder = tabSegment.tabBuilder();
//        builder.skinChangeWithTintColor(false);

        QMUITab home = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_home))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_home_selected))
                .setText("首页")
                .setTextSize(QMUIDisplayHelper.sp2px(getContext(), 11), QMUIDisplayHelper.sp2px(getContext(), 13))
                .build(getContext());

        QMUITab msg = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_msg))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_msg_selected))
                .setText("消息")
                .setTextSize(QMUIDisplayHelper.sp2px(getContext(), 11), QMUIDisplayHelper.sp2px(getContext(), 13))
                .build(getContext());

        QMUITab channel = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_channel))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_channel_selected))
                .setText("频道")
                .setTextSize(QMUIDisplayHelper.sp2px(getContext(), 11), QMUIDisplayHelper.sp2px(getContext(), 13))
                .build(getContext());

        QMUITab dynamic = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_dynamic))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_dynamic_selected))
                .setText("动态")
                .setTextSize(QMUIDisplayHelper.sp2px(getContext(), 11), QMUIDisplayHelper.sp2px(getContext(), 13))
                .build(getContext());

        QMUITab mine = builder
                .setNormalDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_mine))
                .setSelectedDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.icon_mine_selected))
                .setText("我的")
                .setTextSize(QMUIDisplayHelper.sp2px(getContext(), 11), QMUIDisplayHelper.sp2px(getContext(), 13))
                .build(getContext());

        tabSegment.addTab(home)
                .addTab(msg)
                .addTab(dynamic)
                .addTab(channel)
                .addTab(mine);
    }

    private void initPagers() {
        //禁止手动左右滑动
        viewPager.setSwipeable(false);

        pagers = new HashMap<>();
        pagers.put(Pager.HOME, new HomeFragment());
        pagers.put(Pager.MESSAGE, new MessageFragment());
        pagers.put(Pager.CHANNEL, new ChannelFragment());
        pagers.put(Pager.DYNAMIC, new DynamicFragment());
        pagers.put(Pager.MINE, new MineFragment());
        viewPager.setAdapter(new QMUIFragmentPagerAdapter(getBaseFragmentActivity().getSupportFragmentManager()) {
            @Override
            public QMUIFragment createFragment(int position) {
                return pagers.get(Pager.getPagerFromPositon(position));
            }

            @Override
            public int getCount() {
                return pagers.size();
            }
        });
        tabSegment.setupWithViewPager(viewPager, false);
        viewPager.setOffscreenPageLimit(5);
    }

    enum Pager {
        CHANNEL, MESSAGE, DYNAMIC, HOME, MINE;

        public static Pager getPagerFromPositon(int position) {
            switch (position) {
                case 0:
                    return HOME;
                case 1:
                    return MESSAGE;
                case 2:
                    return CHANNEL;
                case 3:
                    return DYNAMIC;
                case 4:
                    return MINE;
                default:
                    return HOME;
            }
        }
    }

}
