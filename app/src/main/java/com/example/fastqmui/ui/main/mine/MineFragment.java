package com.example.fastqmui.ui.main.mine;


import com.example.fastqmui.R;
import com.example.fastqmui.base.BaseFragment;
import com.qmuiteam.qmui.widget.QMUITopBarLayout;

import butterknife.BindView;

/**
 * 我的
 */
public class MineFragment extends BaseFragment {
    @BindView(R.id.topbar)
    QMUITopBarLayout topbar;

    @Override
    public void execute() {

    }

    @Override
    public int initLayout() {
        return R.layout.fragment_mine;
    }

    @Override
    public void initTopbar() {

    }
}
