package com.example.fastqmui.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.fastqmui.data.dynamic.domain.Dynamic;
import com.example.fastqmui.data.dynamic.local.DynamicDao;

@Database(entities = {Dynamic.class}, version = 1, exportSchema = false)
public abstract  class AppDatabase extends RoomDatabase {
    public abstract DynamicDao dynamicDao();
}
