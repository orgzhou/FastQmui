package com.example.fastqmui.data.dynamic.remote;

import com.example.fastqmui.data.dynamic.domain.Dynamic;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RemoteDynamicResource {

    @GET("api/customer/list")
    Observable<List<Dynamic>> list(
        @Query("keyword") String keyword,
        @Query("page") int page,
        @Query("size") int size);
}
