package com.example.fastqmui.data;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.CallAdapter;

public class ResponseMessageAdapter<R> implements CallAdapter<R, Object> {

    private final Type responseType;

    public ResponseMessageAdapter(Type responseType) {
        this.responseType = responseType;
    }

    @Override
    public Type responseType() {
        return responseType;
    }

    @Override
    public Object adapt(Call<R> call) {
        return null;
    }
}
