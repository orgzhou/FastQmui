package com.example.fastqmui.data.dynamic;

import com.example.fastqmui.data.dynamic.domain.Dynamic;
import com.example.fastqmui.data.dynamic.remote.RemoteDynamicResource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DynamicRepository {

    private RemoteDynamicResource remoteDynamicResource;

    @Inject
    public DynamicRepository(RemoteDynamicResource remoteDynamicResource) {
        this.remoteDynamicResource = remoteDynamicResource;
    }


    public Observable<List<Dynamic>> list(String keyword, int page, int size) {
        return remoteDynamicResource.list(keyword, page, size);
    }
}
