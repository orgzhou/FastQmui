package com.example.fastqmui.data.dynamic.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.fastqmui.data.dynamic.domain.Dynamic;

import java.util.List;

import io.reactivex.Observable;

@Dao
public interface DynamicDao {
    @Query("SELECT * FROM Dynamic")
    Observable<List<Dynamic>> listDynamic();

    @Insert
    void insertAll(Dynamic... dynamics);

    @Delete
    void delete(Dynamic code);

    @Query("DELETE FROM Dynamic")
    void deleteAll();
}
