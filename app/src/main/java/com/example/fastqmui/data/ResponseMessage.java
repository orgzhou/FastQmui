package com.example.fastqmui.data;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ResponseMessage<T> {
    private Header header = new Header();

    private T body;

    private ResponseMessage object;

    public T getResult() {
        return body;
    }

    public Type getTParameterizedType() {
        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            Type[] types = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();
            if (types.length > 0) {
                return types[0];
            }
        } else {
            return new TypeToken<T>() {
            }.getType();
        }
        return null;
    }

    class Header {
        private String status;
        private String statusCode;
        private String statusMsg;
        private String rpcId;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

        public String getRpcId() {
            return rpcId;
        }

        public void setRpcId(String rpcId) {
            this.rpcId = rpcId;
        }
    }
}
