package com.example.fastqmui.di;

import com.example.fastqmui.data.dynamic.remote.RemoteDynamicResource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import retrofit2.Retrofit;

/**
 *   资源库依赖
 */
@Module
@InstallIn(ApplicationComponent.class)
public class DataModule {

    @Provides
    @Singleton
    static RemoteDynamicResource remoteDynamicResource(Retrofit retrofit){
        return retrofit.create(RemoteDynamicResource.class);
    }
}
