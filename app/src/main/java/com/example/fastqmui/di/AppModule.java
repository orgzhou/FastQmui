package com.example.fastqmui.di;

import android.content.Context;
import android.text.TextUtils;

import androidx.room.Room;

import com.example.fastqmui.AppConstant;
import com.example.fastqmui.base.converter.LenientGsonConverterFactory;
import com.example.fastqmui.data.AppDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.luck.picture.lib.tools.SPUtils;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.qualifiers.ApplicationContext;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * global dependence
 */
@Module
@InstallIn(ApplicationComponent.class)
public class AppModule {

//    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("DROP TABLE IF EXISTS `CheckItem`");
//            database.execSQL("CREATE TABLE IF NOT EXISTS `CheckItem` (`id` TEXT NOT NULL, `custCheckRecordId` TEXT NOT NULL, `custDeviceId` TEXT, `custDeviceType` TEXT, `name` TEXT, `type` INTEGER, `rangeStart` REAL, `rangeEnd` REAL, `discStd` TEXT, `value` REAL, `defect` INTEGER NOT NULL, `updated` INTEGER NOT NULL, `elecRoomId` TEXT, PRIMARY KEY(`id`, `custCheckRecordId`))");
//        }
//    };


    @Singleton
    @Provides
    AppDatabase providesRoomDatabase(@ApplicationContext Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "fastqmui-db")
//                .addMigrations(MIGRATION_2_3)
                .fallbackToDestructiveMigration()
                .build();
    }

//    @Provides
//    @Singleton
//    static AuthInterceptor AuthInterceptor(){
//        return AuthInterceptor.getInstance();
//    }

    private static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
//        .addInterceptor(AuthInterceptor())
        .connectTimeout(50, TimeUnit.SECONDS)
        .readTimeout(50, TimeUnit.SECONDS)
        .build();

    @Provides
    @Singleton
    static Retrofit retrofit(@ApplicationContext Context context){
        Gson gson = new GsonBuilder()
            .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    return new Date(json.getAsJsonPrimitive().getAsLong());
                }
            })
            .setDateFormat(AppConstant.DATE_FORMAT_STYLE)
            .create();
        return new Retrofit.Builder()
                .baseUrl(AppConstant.BASIC_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(LenientGsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }



}
