package com.example.fastqmui.di;

import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
public abstract class ServiceModule {

//    @ContributesAndroidInjector
//    abstract AuthenticatorService authenticatorServiceInjector();
}
