package com.example.fastqmui.base.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.qmuiteam.qmui.skin.QMUISkinHelper;
import com.qmuiteam.qmui.skin.QMUISkinValueBuilder;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.util.QMUIResHelper;
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopups;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.fastqmui.R;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class WorkUtils {
    private static final String TAG = "WorkUtils";
    public static int mCurrentDialogStyle = com.qmuiteam.qmui.R.style.QMUI_Dialog;

    /**
     * 判断是否属于多次点击
     */
    private static long lastClickTime;
    public static boolean isFastDoubleClick(long second) {
        long time = System.currentTimeMillis();
        if ( time - lastClickTime < second) {
            return false;
        }
        lastClickTime = time;
        return true;
    }


    public static QMUITipDialog showQMUITipDialog(Context context, String title, int type, boolean show) {
        QMUITipDialog qmuiTipDialog = new QMUITipDialog.Builder(context)
            .setIconType(type)
            .setTipWord(title)
            .create();
        if (show) {
            qmuiTipDialog.show();
        }
        return qmuiTipDialog;
    }

    public static QMUITipDialog showQMUITipDialog(Context context, String title, int type, long millisecond) {
        QMUITipDialog tipDialog = new QMUITipDialog.Builder(context)
                .setIconType(type)
                .setTipWord(title)
                .create();

        new Handler().postDelayed(()->{
            if (tipDialog != null && tipDialog.isShowing()) {
                tipDialog.dismiss();
            }
        }, millisecond);
        return tipDialog;
    }

    public static QMUITipDialog showQMUITipDialog(Context context, String title, int type, long millisecond, Hook hook) {
        QMUITipDialog tipDialog = new QMUITipDialog.Builder(context)
                .setIconType(type)
                .setTipWord(title)
                .create();

        new Handler().postDelayed(()->{
            if (tipDialog != null && tipDialog.isShowing()) {
                tipDialog.dismiss();
                hook.onClick();
            }
        }, millisecond);
        return tipDialog;
    }

    public static QMUIDialog showQMUIDialog(Activity activity, String title, String message, int dialogStyle, QMUIDialogAction.ActionListener confirm) {
        return new QMUIDialog.MessageDialogBuilder(activity)
            .setTitle(title)
            .setMessage(message)
            .addAction("取消", (dialog, index) -> dialog.dismiss())
            .addAction(0, "确定", dialogStyle, confirm)
            .create(mCurrentDialogStyle);
    }
    public static QMUIDialog showQMUIDialog(
            Activity activity,
            String title,
            String message,
            int dialogStyle,
            QMUIDialogAction.ActionListener confirm,
            QMUIDialogAction.ActionListener cancle) {
        return new QMUIDialog.MessageDialogBuilder(activity)
//            .setTitle(title)
                .setMessage(message)
                .addAction("取消", cancle)
                .addAction(0, "确定", dialogStyle, confirm)
                .create(mCurrentDialogStyle);
    }

    /*public static QMUIDialog showQMUIDialog(Activity activity, String title, String message, int dialogStyle, QMUIDialogAction.ActionListener confirm, QMUIDialogAction.ActionListener dismiss) {
        return new QMUIDialog.MessageDialogBuilder(activity)
                .setTitle(title)
                .setMessage(message)
                .addAction("取消", dismiss)
                .addAction(0, "确定", dialogStyle, confirm)
                .create(mCurrentDialogStyle);
    }

    public static QMUIDialog showQMUIDialog(Activity activity, String title, String message, int dialogStyle, String confirmText, String cancelText, QMUIDialogAction.ActionListener confirm, QMUIDialogAction.ActionListener cancel) {
        return new QMUIDialog.MessageDialogBuilder(activity)
                .setTitle(title)
                .setMessage(message)
                .addAction(cancelText, cancel)
                .addAction(0, confirmText, dialogStyle, confirm)
                .create(mCurrentDialogStyle);
    }*/

    public static String dateFormat(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        if(date == null){
            return "--";
        }else{
            return sdf.format(date);
        }
    }
    /**
     * 高亮
     * @param textView
     * @param specifiedText
     */
    public static void setSpecifiedText(TextView textView, String specifiedText) {
        String[] keyword = new String[]{specifiedText};
        SpannableStringBuilder spannable = new SpannableStringBuilder(textView.getText().toString());
        CharacterStyle span;
        String wordReg;
        for (int i = 0; i < keyword.length; i++) {
            String key = "";
            //  处理通配符问题
            if (keyword[i].contains("*") || keyword[i].contains("(") || keyword[i].contains(")")) {
                char[] chars = keyword[i].toCharArray();
                for (int k = 0; k < chars.length; k++) {
                    if (chars[k] == '*' || chars[k] == '(' || chars[k] == ')') {
                        key = key + "\\" + String.valueOf(chars[k]);
                    } else {
                        key = key + String.valueOf(chars[k]);
                    }
                }
                keyword[i] = key;
            }

            wordReg = "(?i)" + keyword[i];   //忽略字母大小写

            Pattern pattern = Pattern.compile(wordReg);
            Matcher matcher = pattern.matcher(textView.getText().toString());
            while (matcher.find()) {
                span = new ForegroundColorSpan(Color.parseColor("#e81c1c"));
                spannable.setSpan(span, matcher.start(), matcher.end(), Spannable.SPAN_MARK_MARK);
            }
        }
        textView.setText(spannable);
    }

    /**
     * 设置SearchView下划线透明
     * @param searchView
     */
    public static void setUnderLinetransparent(SearchView searchView){
        try {
            Class<?> argClass = searchView.getClass();
            // mSearchPlate是SearchView父布局的名字
            Field ownField = argClass.getDeclaredField("mSearchPlate");
            ownField.setAccessible(true);
            View mView = (View) ownField.get(searchView);
            mView.setBackgroundColor(Color.TRANSPARENT);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 遮罩效果信息弹窗
     * @param context
     * @param v
     * @param content
     * @param onDismissListener
     */
    public static void showQMUIPopupsDialog(Context context, View v, String content, int width, PopupWindow.OnDismissListener onDismissListener) {
        TextView textView = new TextView(context);
        textView.setLineSpacing(QMUIDisplayHelper.dp2px(context, 4), 1.0f);
        int padding = QMUIDisplayHelper.dp2px(context, 20);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(padding, padding, padding, padding);
        textView.setText(content);
        textView.setTextColor(QMUIResHelper.getAttrColor(context, R.attr.app_skin_common_title_text_color));
        QMUISkinValueBuilder builder = QMUISkinValueBuilder.acquire();
        builder.textColor(R.attr.app_skin_common_title_text_color);
        QMUISkinHelper.setSkinValue(textView, builder);
        builder.release();
        QMUIPopups.popup(context, QMUIDisplayHelper.dp2px(context, width))
            .preferredDirection(QMUIPopup.DIRECTION_BOTTOM)
            .view(textView)
            .edgeProtection(QMUIDisplayHelper.dp2px(context, 20))
            .dimAmount(0.3f)
            .animStyle(QMUIPopup.ANIM_GROW_FROM_CENTER)
            .onDismiss(onDismissListener)
            .show(v);
    }

    /**
     * 校验控件是否有值
     * @param context
     * @param views
     * @return
     */
    public static boolean validateViewHasValue(Context context, Map<View, String> views) {
        for (Map.Entry<View, String> viewEntry : views.entrySet()) {
            View key = viewEntry.getKey();
            String value = viewEntry.getValue();

            if (key instanceof  EditText) {
                EditText editText = (EditText) key;
                if ("".equals(editText.getText().toString().trim())) {
                    WorkUtils.showQMUIPopupsDialog(context, key, value, 250, null);
                    return false;
                }
            } else if (key instanceof TextView) {
                TextView textView = (TextView) key;
                if ("".equals(textView.getText().toString().trim())) {
                    WorkUtils.showQMUIPopupsDialog(context, key, value, 250, null);
                    return false;
                }
            }
        }
        return true;
    }

    public static MultipartBody filesToMultipartBody(File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        // TODO: 16-4-2  这里为了简单起见，没有判断file的类型
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/png"), file);
        builder.addFormDataPart("file", file.getName(), requestBody);
        builder.setType(MultipartBody.FORM);
        MultipartBody multipartBody = builder.build();
        return multipartBody;
    }

    /**
     * 隐藏软键盘(只适用于Activity，不适用于Fragment)
     */
    public static void hideSoftKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * 隐藏软键盘(可用于Activity，Fragment)
     */
    public static void hideSoftKeyboard(Context context, List<View> viewList) {
        if (viewList == null) return;

        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);

        for (View v : viewList) {
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    // ================================ 生成不同类型的BottomSheet
    public static void showSimpleBottomSheetList(boolean gravityCenter,
                                                 boolean addCancelBtn,
                                                 boolean withIcon,
                                                 CharSequence title,
                                                 String[] items,
                                                 boolean allowDragDismiss,
                                                 boolean withMark,
                                                 QMUIBottomSheet.BottomListSheetBuilder.OnSheetItemClickListener listener, Activity activity) {
        QMUIBottomSheet.BottomListSheetBuilder builder = new QMUIBottomSheet.BottomListSheetBuilder(activity);
        builder.setGravityCenter(gravityCenter)
                .setTitle(title)
                .setAddCancelBtn(addCancelBtn)
                .setAllowDrag(allowDragDismiss)
                .setNeedRightMark(withMark)
                .setOnSheetItemClickListener(listener);
        for (String item : items) {
            builder.addItem(item);
        }
        builder.build().show();
    }

    // ================================ 生成不同类型的BottomSheet
    public static void showSimpleBottomSheetList(boolean gravityCenter,
                                                 boolean addCancelBtn,
                                                 boolean withIcon,
                                                 CharSequence title,
                                                 String[] items,
                                                 boolean allowDragDismiss,
                                                 boolean withMark,
                                                 QMUIBottomSheet.BottomListSheetBuilder.OnSheetItemClickListener listener, Activity activity,
                                                 String select) {
        QMUIBottomSheet.BottomListSheetBuilder builder = new QMUIBottomSheet.BottomListSheetBuilder(activity);
        builder.setGravityCenter(gravityCenter)
                .setTitle(title)
                .setAddCancelBtn(addCancelBtn)
                .setAllowDrag(allowDragDismiss)
                .setNeedRightMark(true)
                .setOnSheetItemClickListener(listener);

        //设置成-1,默认不标记是谁被选中了，后面的循环判断
        builder.setCheckedIndex(-1);

        if (select != null && !"".equals(select)) {
            for (int i = 0 ; i < items.length; i++) {
                if (select.equals(items[i])) {
                    builder.setCheckedIndex(i);
                    break;
                }
            }
        }
        for (String item : items) {
            builder.addItem(item);
        }
        builder.build().show();
    }

    /**
     * 包装类型转string
     * @return
     */
    public static String packageDataToString(Integer integer) {
        if (integer != null) {
            return String.valueOf(integer);
        } else {
            return "";
        }
    }
    public static String packageDataToString(Float floatValue) {
        if (floatValue != null) {
            return String.valueOf(floatValue);
        } else {
            return "";
        }
    }

    public static String getContentByValue(Map<String, String> map, String value) {
        if (value != null && !"".equals(value)) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getValue().equals(value)) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    public static boolean isNumber(String str) {
        String reg = "^[0-9]+(.[0-9]+)?$";
        return str.matches(reg);
    }
}
