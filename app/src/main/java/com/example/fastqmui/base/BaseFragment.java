/*
 * Tencent is pleased to support the open source community by making QMUI_Android available.
 *
 * Copyright (C) 2017-2018 THL A29 Limited, a Tencent company. All rights reserved.
 *
 * Licensed under the MIT License (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.fastqmui.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.fastqmui.App;
import com.example.fastqmui.base.manager.QDUpgradeManager;
import com.qmuiteam.qmui.arch.QMUIFragment;
import com.qmuiteam.qmui.arch.SwipeBackLayout;
import com.qmuiteam.qmui.skin.QMUISkinMaker;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;

import butterknife.ButterKnife;

public abstract class BaseFragment extends QMUIFragment {

    private int mBindId = -1;

    @Override
    protected View onCreateView() {
        int layoutId = initLayout();
        View rootView = LayoutInflater.from(getContext()).inflate(layoutId, null);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(App.openSkinMake){
            openSkinMaker();
        }

//        //加载数据
//        initData();
//        //初始化观察者LiveData
//        initLiveData();
//        //初始化topbar
//        initTopbar();
//        //初始化View
//        initViews();
//        //初始化View的值
//        initViewsValue();
//        //初始化ViewEvent
//        initViewsEvent();
//        //start返回值
//        initResult();

        execute();
    }

    public abstract void execute();
    public abstract int initLayout();

    public void initTopbar() {}
    public void initLiveData() {}
    public void initData() {}
    public void initViews() {}
    public void initViewsValue() {}
    public void initViewsEvent() {}
    public void initResult() {}

    //打开Fragment的渐变动画
    @Override
    public TransitionConfig onFetchTransitionConfig() {
        return SLIDE_TRANSITION_CONFIG;
    }

    /**
     * 左滑手势返回动画
     * @return
     */
    @Override
    protected SwipeBackLayout.ViewMoveAction dragViewMoveAction() {
        return SwipeBackLayout.MOVE_VIEW_LEFT_TO_RIGHT;
    }

    public void openSkinMaker(){
        if(mBindId < 0){
            mBindId = QMUISkinMaker.getInstance().bind(this);
        }
    }

    public void closeSkinMaker(){
        QMUISkinMaker.getInstance().unBind(mBindId);
        mBindId = -1;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        closeSkinMaker();
    }

    @Override
    protected int backViewInitOffset(Context context, int dragDirection, int moveEdge) {
        if (moveEdge == SwipeBackLayout.EDGE_TOP || moveEdge == SwipeBackLayout.EDGE_BOTTOM) {
            return 0;
        }
        return QMUIDisplayHelper.dp2px(context, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        QDUpgradeManager.getInstance(getContext()).runUpgradeTipTaskIfExist(getActivity());
    }

    @Override
    public Object onLastFragmentFinish() {
        return null;
    }

}

