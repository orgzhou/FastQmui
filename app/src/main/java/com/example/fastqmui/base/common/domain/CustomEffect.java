package com.example.fastqmui.base.common.domain;

import com.qmuiteam.qmui.arch.effect.Effect;

public class CustomEffect extends Effect {
    private final Object mContent;

    public CustomEffect(Object content){
        mContent = content;
    }

    public Object getContent() {
        return mContent;
    }
}
