package com.example.fastqmui

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import com.example.fastqmui.base.manager.QDSkinManager
import com.example.fastqmui.base.utils.ErrorHandler
import com.qmuiteam.qmui.arch.QMUISwipeBackActivityManager
import com.qmuiteam.qmui.skin.QMUISkinMaker
import com.squareup.leakcanary.LeakCanary
import com.taobao.sophix.SophixManager
import com.tencent.bugly.Bugly
import dagger.hilt.android.HiltAndroidApp
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject

@HiltAndroidApp
class App : Application() , Configuration.Provider{
    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override fun onCreate() {
        super.onCreate()
        self = this

        //初始化QMUISwipeBackActivityManager，否则点击屏幕时就程序就会崩溃
        QMUISwipeBackActivityManager.init(this)
        RxJavaPlugins.setErrorHandler(ErrorHandler.TOAST_ERROR_HANDLER)

        initLeakCanary()
        initSkinManager()
        initBugly()
        initSophix()
    }

    /**
     * 内存溢出监控
     */
    private fun initLeakCanary(){
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this)
    }

    private fun initSkinManager(){
        QDSkinManager.install(this)
        QMUISkinMaker.init(self, arrayOf("com.qmuiteam.qmuidemo"), arrayOf("app_skin_"), R.attr::class.java)
    }

    /**
     * emas 热更新
     */
    private fun initSophix() {
        //下载补丁 queryAndLoadNewPatch不可放在attachBaseContext 中，否则无网络权限，建议放在后面任意时刻，如onCreate中
        SophixManager.getInstance().queryAndLoadNewPatch()
    }

    /**
     * bugly全量升级+异常监控
     */
    private fun initBugly() {
        Bugly.init(applicationContext, "4e70a8be04", true)
    }

    companion object {
        @JvmField
        var openSkinMake : Boolean = true
        @JvmField
        var self: App? = null

        //emas key+秘钥
        const val DEFAULT_APPKEY = "xxxx"
        const val DEFAULT_APPSECRET = "xxxxx"
    }

    override fun getWorkManagerConfiguration() = Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()
}